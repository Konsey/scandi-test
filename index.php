<?php
    require_once 'php/products.php';
    require_once 'php/db.php';
?>
<!DOCTYPE HTML>
<html>
    <head>  
        <meta charset="UTF-8">
        <title>
            Product List
        </title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/list.js"></script>
        <link rel="stylesheet" href="css/reset.css"/>
        <link rel="stylesheet" href="css/style_list.css"/>
    </head>
    <body>
        <div class = "topSection">
            <div class="topbar">
                <h1>Product List</h1>
                <a href="product_add.html">
                    <input type="button" value="ADD">
                </a>
                <input type="button" value="MASS DELETE" onclick=massDelete()>
            </div>
            <hr>
        </div>
        <div class="productGrid">
            <?php
                foreach($mydb->getAllDvds() as $dvd){
                    echo"<div id=\"dvds_".$dvd->getSku()."\">
                        <input type=\"checkbox\">
                        <h1>".$dvd->getSku()."</h1>
                        <p>".$dvd->getName()."</p>
                        <p>".$dvd->getPrice()."</p>
                        <p>Size: ".$dvd->getSize()."</p>
                        </div>";
                }
                foreach($mydb->getAllBooks() as $book){
                    echo"<div id=\"books_".$book->getSku()."\">
                        <input type=\"checkbox\">
                        <h1>".$book->getSku()."</h1>
                        <p>".$book->getName()."</p>
                        <p>".$book->getPrice()."</p>
                        <p>Weight: ".$book->getWeight()."</p>
                        </div>";
                }
                foreach($mydb->getAllFurniture() as $currFurniture){
                    echo"<div id=\"furniture_".$currFurniture->getSku()."\">
                        <input type=\"checkbox\">
                        <h1>".$currFurniture->getSku()."</h1>
                        <p>".$currFurniture->getName()."</p>
                        <p>".$currFurniture->getPrice()."</p>
                        <p>Dimension: ".$currFurniture->getHeight()."x".$currFurniture->getWidth()."x".$currFurniture->getLength()."</p>
                        </div>";
                }
            ?>
        </div>
        <hr>
    </body>
</html>