function massDelete(){
    var Products = $(".productGrid div");
    
    var selectedProducts =[];
    var amount=0;
    for(var i=0; i<Products.length;i++){
        if(Products[i].getElementsByTagName('input')[0].checked){
            selectedProducts.push(Products[i]);
        }
    }
    var elementsToDelete = "";
    for(var i=0; i<selectedProducts.length;i++){
        if(elementsToDelete!=""){
            elementsToDelete+="-"+selectedProducts[i].id;
        }
        else{
            elementsToDelete+=selectedProducts[i].id;
        }
    }
    if(elementsToDelete.length==0){
        return;
    }
    $.ajax({
        url: "php/delete_listing.php",
        type: "POST",
        data:{"toDelete":elementsToDelete},
        success: function(data) {
            $("body").html(data);
         }
    });
}
$(function(){
    $('input:checkbox').prop('checked',false);
});