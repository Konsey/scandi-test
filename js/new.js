function dvdData(){
    return `
        <label for=\"size\">Size (MB)</label>
        <input type=\"text\" name=\"size\" id=\"size\">
    <p>Please, provide size</p>
        `;
}
function bookData(){
    return `
        <label for=\"weight\">Weight (KG)</label>
        <input type=\"text\" name=\"weight\" id=\"weight\">
        <p>Please, provide weight</p>
        `;
}
function furnitureData(){
    return  `
        <label for=\"height\">Height (CM)</label>
        <input type=\"text\" name=\"height\" id=\"height\">
        <label for=\"width\">Width (CM)</label>
        <input type=\"text\" name=\"width\" id=\"width\">
        <label for=\"length\">Length (CM)</label>
        <input type=\"text\" name=\"length\" id=\"length\">
        <p>Please, provide dimensions</p>
        `;
}
function typeData(type){
    $("#typeData").html(window[type+"Data"]());
}
function dvdCheck(){
    if($("input[name=\"size\"]").val()==""){
        alert("Please, submit required data");
        return false;
    }
    if($("input[name=\"size\"]").val().length>15){
        alert("Size can contain no more than 15 characters");
        return false;
    }
    if(isNaN($("input[name=\"size\"]").val())){
        alert("Please, provide the data of indicated type");
        return false;
    }
    return true;
}
function bookCheck(){
    if($("input[name=\"weight\"]").val()==""){
        alert("Please, submit required data");
        return false;
    }
    if($("input[name=\"weight\"]").val().length>20){
        alert("Weight can contain no more than 20 characters");
        return false;
    }
    if(isNaN($("input[name=\"weight\"]").val())){
        alert("Please, provide the data of indicated type");
        return false;
    }
    return true;
}
function furnitureCheck(){
    if($("input[name=\"height\"]").val()==""){
        alert("Please, submit required data");
        return false;
    }
    if($("input[name=\"width\"]").val()==""){
        alert("Please, submit required data");
        return false;
    }
    if($("input[name=\"length\"]").val()==""){
        alert("Please, submit required data");
        return false;
    }
    if($("input[name=\"height\"]").val().length>10){
        alert("Dimensions can contain no more than 10 characters");
        return false;
    }
    if($("input[name=\"width\"]").val().length>10){
        alert("Dimensions can contain no more than 10 characters");
        return false;
    }
    if($("input[name=\"length\"]").val().length>10){
        alert("Dimensions can contain no more than 10 characters");
        return false;
    }
    if(isNaN($("input[name=\"height\"]").val())){
        alert("Please, provide the data of indicated type");
        return false;
    }
    if(isNaN($("input[name=\"width\"]").val())){
        alert("Please, provide the data of indicated type");
        return false;
    }
    if(isNaN($("input[name=\"length\"]").val())){
        alert("Please, provide the data of indicated type");
        return false;
    }
    return true;
}
window.addEventListener('DOMContentLoaded', (event) => {
    $("#productType").change(function(){
        var selectedType=$("#productType").val();
        typeData(selectedType);
    });
    $("#submitSpecs").click(function(){
        if($("input[name=\"sku\"]").val()==""){
            alert("Please, submit required data");
            return;
        }
        if($("input[name=\"price\"]").val()==""){
            alert("Please, submit required data");
            return;
        }
        if($("input[name=\"name\"]").val()==""){
            alert("Please, submit required data");
            return;
        }
        if($("input[name=\"sku\"]").val().length>30){
            alert("SKU can contain no more than 30 characters");
            return;
        }
        if($("input[name=\"sku\"]").val().indexOf('-')>=0 || $("input[name=\"sku\"]").val().indexOf('_')>=0){
            alert("SKU cannot contain \'-\' or \'_\' symbols...");
            return;
        }
        if($("input[name=\"price\"]").val().length>10){
            alert("Price can contain no more than 10 characters");
            return;
        }
        if($("input[name=\"name\"]").val().length>30){
            alert("Name can contain no more than 30 characters");
            return;
        }
        if(isNaN($("input[name=\"price\"]").val())){
            alert("Please, provide the data of indicated type");
            return;
        }
        if(!window[$("#productType").val()+"Check"]()){
            return;
        }
        $("#product_form").submit();
    });
    $(function() {
        $("#typeData").html(dvdData());
    });
});
