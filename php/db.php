<?php
    class db{
        private $myconn;
        private $servername = "localhost";
        private $username = "id17094496_scandiuser";
        private $password = "scandiuserScandiuser1!";
        private $database = "id17094496_scandidb";
        public function __construct(){
            $this->myconn = new mysqli($this->servername,$this->username,$this->password,$this->database);
        }
        public function getConnection(){
            return $this->myconn;
        }
        public function deleteRecord($tableToDelete, $deleteSku){
            $result = mysqli_query($this->getConnection(),"SELECT * FROM ".$tableToDelete." WHERE sku='".$deleteSku."'");
            if(!$row = $result->fetch_assoc()){
                echo"not found";
            }
            if($row['quantity']>1){
                mysqli_query($this->getConnection(),"UPDATE ".$tableToDelete." SET quantity = quantity-1 where sku = '".$deleteSku."'") or die(mysqli_error($this->getConnection()));
            }
            else{
                mysqli_query($this->getConnection(),"DELETE FROM ".$tableToDelete." WHERE sku='".$deleteSku."'");
            }
        }
        public function getAllDvds(){
            $dvds =[];
            $result = mysqli_query($this->getConnection(),"SELECT * FROM dvds");
            while($row = $result->fetch_assoc()){
                while($row['quantity']>0){
                    $dvd = new Dvd();
                    $dvd->getSpecsFrom($row);
                    array_push($dvds, $dvd);
                    $row['quantity']--;
                }
            }
            return $dvds;
        }
        public function getAllBooks(){
            $books =[];
            $result = mysqli_query($this->getConnection(),"SELECT * FROM books");
            while($row = $result->fetch_assoc()){
                while($row['quantity']>0){
                    $book = new Book();
                    $book->getSpecsFrom($row);
                    array_push($books, $book);
                    $row['quantity']--;
                }
            }
            return $books;
        }
        public function getAllFurniture(){
            $furniture =[];
            $result = mysqli_query($this->getConnection(),"SELECT * FROM furniture");
            while($row = $result->fetch_assoc()){
                while($row['quantity']>0){
                    $currFurniture = new Furniture();
                    $currFurniture->getSpecsFrom($row);
                    array_push($furniture, $currFurniture);
                    $row['quantity']--;
                }
            }
            return $furniture;
        }
    }
    $mydb=new db();
?>