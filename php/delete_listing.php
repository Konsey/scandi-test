<?php
    require_once 'products.php';
    require_once 'db.php';
    if(isset($_POST['toDelete']) and $_POST['toDelete']!=""){
        $elementsToDelete = $_POST['toDelete'];
        $elementsIndividual = explode("-",$elementsToDelete);
        foreach($elementsIndividual as $element){
            $elementMap = explode("_",$element);
            $mydb->deleteRecord($elementMap[0],$elementMap[1]);
        }
        header("Location: ../index.php");
        exit;
    }
    header("Location: ../index.php");
    exit;
?>